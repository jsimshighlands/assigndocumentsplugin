﻿using System;
using System.Xml;
using System.IO;
using EllieMae.Encompass.ComponentModel;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.BusinessObjects.Loans;
using EllieMae.Encompass.BusinessObjects.Loans.Logging;
using EllieMae.Encompass.Client;

namespace AssignDocumentsPlugin
{
    [Plugin]
    public class AssignDocuments
    {
        public AssignDocuments()
        {
            EncompassApplication.LoanOpened += new EventHandler(Application_LoanOpened);
        }

        public void Application_LoanOpened(object source, EventArgs e)
        {
            try
            {
                //Get a list of all attachments that are currently in the Loan file
                LoanAttachments eFolderDocs = EncompassApplication.CurrentLoan.Attachments;
                //Load the AssignDocuments mapping file
                var xmlAssignDocs = EncompassApplication.CurrentLoan.Session.DataExchange.GetCustomDataObject("AssignDocuments.xml").OpenStream();

                /*Open the xmlDocument and loop through the nodes
                 *This will run as many times as there are document lines in the xml file
                 *To minimize the impact to loan open time make sure to keep the document
                 *list to a minimum.
                */
                using (var assignDocsReader = new StreamReader(xmlAssignDocs))
                {
                    var xmlDoc = new XmlDocument();

                    xmlDoc.Load(assignDocsReader);

                    var docNodes = xmlDoc.SelectNodes("//Documents/Document");

                    //Looping through documents in the mapping file
                    foreach (XmlNode docNode in docNodes)
                    {
                        //For each documet in the mapping file loop through the current loan attachements
                        foreach (Attachment att in eFolderDocs)
                        {
                            //When a match is found look to see if the attachment has been assigned a placeholder if not
                            //then add it to the placeholder that is defined in the mapping file.
                            if (att.Title.ToLower().Contains(docNode.Attributes["Name"].Value.ToLower()))
                            {
                                if (att.GetDocument() == null)
                                {
                                    var documents = EncompassApplication.CurrentLoan.Log.TrackedDocuments.GetDocumentsByTitle(docNode.Attributes["eFolder"].Value.ToString());
                                    if (documents.Count > 0)
                                    {
                                        TrackedDocument document = (TrackedDocument)documents[0];
                                        document.Attach(att);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                ApplicationLog.WriteDebug("AssignDocumentsPlugin", e.ToString());
            }
        }
    }
}
