# Assign Documents Plugin

## Plugin Overview

* Description:
  * Plugin designed to assign documents in the File Manager to encompass placeholder as defined in the AssignDocuments.xml file.
* Git Repo:
  * [Bitbucket Repo](https://bitbucket.org/jsimshighlands/assigndocumentsplugin/src/master/)
* Dependencies:
  * Custom Data Object: AssignDocuments.xml
    * Provides the mapping for document names to encompass placeholders.

## How it works

When a loan is opened the plugin looks at the list of documents in the AssignedDocuments.xml. If a document in the File Manager section of the eFolder is found that contains the document listed in the AssignedDOcuments.xml it will place the unassigned document into the placeholder specified in the AssignedDocuments.xml.

### Example

If the AssignedDocuments.xml file had the following listing, <Document Name="_test" eFolder="Junk (Do not ship)"/>. Then when a loan is opened if a document in the file manager contains the name _test then it will be placed in the "Junk (Do not ship)" placeholder.

## Best Practices

Because this is having to look through all attachments it is best to keep the number of documents added to placeholders in this way to a minimum. Otherwise you run the risk of causing loan open times to go up.

## Issues/Workaround

N/A for now.

## Future Improvements

Creat a custom form that will allow for the XML to be modified via Encompass instead of having to be downloaded and changed.